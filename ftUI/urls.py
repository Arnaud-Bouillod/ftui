from django.conf.urls import include
from django.contrib import admin
from django.urls import path, re_path

import ftUI.views as views

urlpatterns = [
    path('admin/', admin.site.urls),

    re_path(r'', include('trade.urls')),

    re_path(r'^$', views.HomeView.as_view(), name="home"),
]
