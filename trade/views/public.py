import json

from django.conf import settings
from django.urls import reverse_lazy
from django.views.generic import FormView, TemplateView

from trade.forms import BackTestingForm


class BackTestingFormView(FormView):

    template_name = "back_testing.html"
    form_class = BackTestingForm
    success_url = reverse_lazy("home")

    def get_file(self):
        with open(settings.CONFIG_FILE) as f:
            info_json = json.load(f)
        return info_json

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        file = self.get_file()
        kwargs['pairs'] = [
            (x, x) for x in file['exchange']['pair_whitelist']
        ]
        return kwargs

    def get_initial(self):
        kwargs = super().get_initial()
        file = self.get_file()
        kwargs['secret_key'] = file['exchange']['secret']
        kwargs['public_key'] = file['exchange']['key']
        return kwargs


class ResultsView(TemplateView):

    template_name = "results.html"


