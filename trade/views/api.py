import io
import json
import os
import re
import shutil
import subprocess

from django.conf import settings
from django.shortcuts import render_to_response
from django.http.response import JsonResponse

from rest_framework.views import APIView


class ResultsAPIView(APIView):

    permission_classes = []

    def get_file(self, filename):
        with open(filename) as f:
            info_json = json.load(f)
        return info_json

    def get_file_and_override(self, secret_key, public_key):
        file = self.get_file(settings.CONFIG_FILE)
        file['exchange']['key'] = public_key
        file['exchange']['secret'] = secret_key
        with io.open(settings.CONFIG_FILE, 'w', encoding='utf-8') as f:
            f.write(json.dumps(file, ensure_ascii=False))
        return None

    def post(self, request, *args, **kwargs):
        # os.remove("ui/templates/plot.html")
        post = request.data
        self.get_file_and_override(post['secret_key'], post['public_key'])
        command = (
            "cd {0};"
            "source {0}.env/bin/activate;"
            "python {0}scripts/plot_dataframe.py --pairs {1} -d {0}"
            "user_data/data/{2}/;"
        ).format(settings.FREQ_PATH, post['pairs'], post['exchange'])
        process = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE)
        lines = []
        html_file = ""
        for line in process.stdout:
            lines.append(str(line))
            tmp = re.findall("/.*\.html", str(line))
            if len(tmp) == 1:
                html_file = tmp[0]
        mv_path = '{}/ui/templates/plot.html'.format(settings.BASE_DIR)
        shutil.move(html_file, mv_path)
        process.wait()

        return render_to_response("plot.html")


class ResultsTabAPIView(APIView):

    permission_classes = []

    def get_file(self, filename):
        with open(filename) as f:
            info_json = json.load(f)
        return info_json

    def post(self, request, *args, **kwargs):
        tab = self.get_file(settings.TAB_FILE)
        if not tab:
            tab = ""
        return JsonResponse({"tab": tab})
