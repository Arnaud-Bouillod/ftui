from django import forms
from django.utils.translation import ugettext_lazy as _

from .models import Trade

__all__ = [
    'BackTestingForm'
]


class BackTestingForm(forms.Form):

    def __init__(self, *args, **kwargs):
        pairs = kwargs.pop('pairs')
        super().__init__(*args, **kwargs)
        self.fields['pairs'] = forms.ChoiceField(choices=pairs, label=_("paire"))

    exchange = forms.ChoiceField(choices=Trade.EXCHANGES, label=_("exchange"))
    secret_key = forms.CharField(max_length=255, label=_("clef secrète"))
    public_key = forms.CharField(max_length=255, label=_("clef publique"))
    period = forms.IntegerField(
        min_value=1, max_value=10, label=_("période (jours)"))
    pairs = forms.ChoiceField(choices=[], label=_("paire"))
    strategies = forms.ChoiceField(
        choices=Trade.STRATEGIES, label=_("stratégies"))


