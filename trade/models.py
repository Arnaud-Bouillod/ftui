

class Trade:

    EXCHANGES = [
        (1, 'Binance'),
        (2, 'Bittrex'),
        (3, 'Coinbase')
    ]

    PAIRS = [
        (1, "ETH/BTC"),
        (2, "AST/BTC"),
        (3, "ETC/BTC"),
        (4, "ETH/BTC"),
        (5, "EOS/BTC"),
        (6, "IOTA/BTC"),
        (7, "LTC/BTC"),
        (8, "MTH/BTC"),
        (9, "NCASH/BTC"),
        (10, "TNT/BTC"),
        (11, "XMR/BTC"),
        (12, "XLM/BTC"),
        (13, "XRP/BTC")
    ]

    STRATEGIES = [
        (1, "Stratégie 1"),
        (2, "Stratégie  2")
    ]
