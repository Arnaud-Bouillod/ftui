from django.urls import re_path

import trade.views.public as views
import trade.views.api as apiviews

urlpatterns = [
    re_path(r'^back-testing/', views.BackTestingFormView.as_view(),
            name="back_testing"),

    re_path(r'^results/', apiviews.ResultsAPIView.as_view(),
            name="results"),

    re_path(r'^results-tab/', apiviews.ResultsTabAPIView.as_view(),
            name="results_tab"),
]
