django==2.2.5
djangorestframework==3.10.3
django-crispy-forms==1.7.2
-e git+https://gitlab.com/Arnaud-Bouillod/freqtrade.git#egg=freqtrade
