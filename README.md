### pré-requis :

- python>=3.6.5

### installation de pip :

`curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py`

`python get-pip.py` (pip doit être mappé sur python>3.6.5)

`pip install --upgrade pip`

### installation du virtualenv :

`(sudo) pip install virtualenv virtualenvwrapper`

`source /usr/local/bin/virtualenvwrapper.sh`

### création du virtualenv :

`mkvirtualenv ftui`

### lancement du virtualenv :

`workon ftui`

### installation des dépendances :

`pip install -r requirements.txt`

### lancement du serveur :

`python manage.py runserver`

### Après avoir build le projet Solidity :

- renseigner la variable `FREQ_PATH` à la fin du fichier settings
- mettre le path du fichier Json `config.json` du projet `freqtrade`


### Dans freqtrade

- `freqtrade backtesting --export trades`
- `freqtrade backtesting`
