$(function() {

    function callApi(data, url, csrf, url2) {
        $.ajax({
            'url': url,
            'method': 'POST',
            'data': JSON.stringify(data),
            'headers': {'X-CSRFToken': csrf},
            'contentType': 'application/json',
        }).done((function(self) {
            return function(response) {
                if (response) {
                    $("div.result").html(response);
                    $.ajax({
                        'url': url2,
                        'method': 'POST',
                        'data': JSON.stringify(data),
                        'headers': {'X-CSRFToken': csrf},
                        'contentType': 'application/json',
                    }).done((function(self) {
                        return function(response) {
                            if (response) {
                                h = response['tab']['header'];
                                headers = "";
                                for (i=0; i<h.length; i++) {
                                   headers += '<th scope="col">' + h[i] + '</th>';
                                }

                                d = response['tab']['data'];
                                datas = "";
                                console.log(d);
                                for (j=0; j<d.length; j++) {
                                    console.log(d[j]);
                                    console.log(d[j].length);
                                    for (k=0; k<d[j].length; k++) {
                                        console.log(d[j][k]);
                                        if (k == 0) {
                                            datas += '<tr><th scope="row">';
                                        }
                                        else {
                                            datas += '<td>';
                                        }
                                        datas += d[j][k];
                                        if (k == 0) {
                                            datas += '</th>';
                                        }
                                        else {
                                            datas += '</td>';
                                        }
                                        if (k == d[j].length - 1)
                                            datas += '</tr>';
                                    }
                                }
                                console.log(datas);
                                html = '<table class="table">' +
                                    '<thead class="thead-dark">' +
                                    '<tr>' + headers + '</tr>' +
                                    '</thead>' +
                                     '<tbody>' + datas + '</tbody>' +
                                    '</table>';
                                $("div.result-tab").html(html);
                            }
                        };
                    })(this));
                }
            };
        })(this));
    }

   $("form.back-test-form button[type=submit]").click(function (e) {
        $('.result').html('<img src="' + $(this).data('loading') + '" alt="Loading..." />');
        var form = $(this).closest('form');

        var exchange = form.find("[name=exchange] option:selected").text();
        var pairs = form.find("[name=pairs] option:selected").text();
        var strategies = form.find("[name=strategies] option:selected").text();
        var period = form.find("[name=period]").val();
        var secret_key = form.find("[name=secret_key]").val();
        var public_key = form.find("[name=public_key]").val();

        var csrf = form.find('input[name=csrfmiddlewaretoken]').val();
        var url = $(this).attr('data-api-call');
       var url2 = $(this).attr('data-api-call-tab');

        var data = {
            "exchange": exchange,
            "pairs": pairs,
            "strategies": strategies,
            "period": period,
            "secret_key": secret_key,
            "public_key": public_key
        }

        callApi(data, url, csrf, url2);

        e.preventDefault();
    })
});